@extends('Layout.app')

@section('title','Inicio de sesión')

@push('css-style-head')
<style lang="scss" scoped>

#app{
     background-image: url('img/fondo-login.jpg');
     background-position: left top;

}
html { overflow:hidden!important }
</style>
@endpush

@push('js-script-body')
    <script src="/js/app-portal-login.js"></script>
@endpush

