<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">

    <style>
        html { overflow:hidden!important }
    </style>

    @stack('css-style-head')

    <!-- Scripts -->
    @stack('js-script-head')
</head>
<body>
    <div id="app"></div>

    <!-- Scripts -->
    @stack('js-script-body')

</body>
</html>
