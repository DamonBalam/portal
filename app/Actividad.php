<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Actividad extends Model
{
    protected $table = 'actividades';

    protected $guarded = ['id'];

    protected $appends = ['codigo'];

    public function getCodigoAttribute()
    {
        return sprintf('AE%d%s',
            $this->etapa,
            $this->consecutivo
        );
    }

    public function ActividadArchivos(){
        return $this->hasMany(ActividadArchivo::class,'actividad_id');
    }

    public function Archivos()
    {
        return $this->belongsToMany(Archivo::class,'actividad_archivos','actividad_id','archivo_id');
    }
}
