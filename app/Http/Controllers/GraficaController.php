<?php

namespace App\Http\Controllers;

use App\Actividad;
use Illuminate\Http\Request;

class GraficaController extends Controller
{
    public function avancesActividades()
    {
        $actividades = Actividad::all();

        $actividadesE1 = $actividades->where('etapa',1);
        $actividadesE2 = $actividades->where('etapa',2);
        $actividadesE3 = $actividades->where('etapa',3);
        $actividadesE4 = $actividades->where('etapa',4);

        $data['totales'] = [0,0];
        $data['avances'] = [0,0,0,0,0,100];

        $data['avances'][0] = $this->decimal($actividadesE1->avg('estatus'));
        $data['avances'][1] = $this->decimal($actividadesE2->avg('estatus'));
        $data['avances'][2] = $this->decimal($actividadesE3->avg('estatus'));
        $data['avances'][3] = $this->decimal($actividadesE4->avg('estatus'));

        $data['totales'][0] = $this->decimal($data['avances'][0] / 100 * 10);
        $data['totales'][0] += $this->decimal($data['avances'][1] / 100 * 25);
        $data['totales'][0] += $this->decimal($data['avances'][2] / 100 * 25);
        $data['totales'][0] += $this->decimal($data['avances'][3] / 100 * 40);

        $data['totales'][1] = $this->decimal(100 - $data['totales'][0]);

        return $data;
    }

    public function decimal($value,$decimal = 2){
        return number_format($value,$decimal);
    }
}
