<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actividad;
use App\Archivo;

class ActividadController extends Controller
{
    public function index()
    {
        return Actividad::with('Archivos')->get();

    }

    public function store(Request $request)
    {
        $data = $request->except('pdf');

        $total = Actividad::whereEtapa($data['etapa'])->count();

        $data['consecutivo'] = str_pad( $total  + 1,3,'0',STR_PAD_LEFT);

        $actividad = Actividad::create($data);

        if ($request->has('pdf')) {
            foreach ($request->file('pdf') as $archivo) {
                $descripcion = sprintf(
                    'Documento %s',
                    $actividad->codigo
                );

                $archivo = $this->registrarArchivo($archivo, 'app/pdfs', $descripcion);
                $actividad->Archivos()->attach($archivo);
            }
        }

        return response()->json(['success'=>true]);
    }


    public function update(Request $request,$id)
    {
        ini_set('max_execution_time', 60);

        $data = $request->except('pdf','existentes');

        $actividad = Actividad::find($id);

        $actividad->update($data);

        $archivos = $request->input('existentes',[]);

        $actividad->ActividadArchivos()->whereIn('archivo_id',$archivos)->delete();

        if ($request->has('pdf')) {
            foreach ($request->file('pdf') as $archivo) {
                $descripcion = sprintf(
                    'Documento %s',
                    $actividad->codigo
                );
    
                $archivo = $this->registrarArchivo($archivo, 'app/pdfs', $descripcion);
                $actividad->Archivos()->attach($archivo);
            }
        }



        return response()->json(['success' => true]);
    }

    public function registrarArchivo($archivo, $carpeta, $descripcion = null, $nombre = null)
    {
        if (is_null($nombre)) {
            $nombre = sprintf('archivo_%s.pdf', time());
        }

        if (is_null($descripcion)) {
            $descripcion = $nombre;
        }

        $ruta = $carpeta . '/' . $nombre;

        $ruta_real = storage_path($ruta);

        $archivo_data = [
            'carpeta'     => $carpeta,
            'nombre'      => $nombre,
            'ruta'        => $ruta,
            'descripcion' => $descripcion,
            'tipo'        => 'pdf',
            'extension'   => $archivo->extension($ruta_real),
            'mimetype'        => $archivo->getMimeType($ruta_real),
            'tamanio'     => $archivo->getSize(),
        ];

        $archivo->move(storage_path($carpeta),$nombre);


        return Archivo::create($archivo_data);
    }

    public function reporte($id)
    {
        $archivo = Archivo::find($id);
        return response()->file(storage_path($archivo->ruta));

    }

}
