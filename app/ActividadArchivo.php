<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActividadArchivo extends Model
{
    protected $table = 'actividad_archivos';
}
