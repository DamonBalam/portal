require('../bootstrap');

import Vue from 'vue'
import vuetify from '../plugins/vuetify'
import Principal from '../views/login.vue'



new Vue({
    el: '#app',
    vuetify,
    components: { Principal },
    template: "<Principal/>",
});
