require('../bootstrap');

import Vue from 'vue'
import store from '../store/store-portal'
import vuetify from '../plugins/vuetify'
import Principal from '../views/index.vue'



new Vue({
    el: '#app',
    store,
    vuetify,
    components: { Principal },
    template: "<Principal/>",
});
