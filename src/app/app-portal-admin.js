require('../bootstrap');

import Vue from 'vue'
import router from "../routes/routes-portal-administrador"
import store from '../store/store-portal-administrador'
import vuetify from '../plugins/vuetify'
import Principal from '../views/indexAdministrador.vue'



new Vue({
    el: '#app',
    store,
    router,
    vuetify,
    components: { Principal },
    template: "<Principal/>",
});
