import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        Actividades:[],
        pastel: {
            tittle: "del Proyecto",
            labels: ["Terminado", "Restante"],
            data: [0,0]
        },
        barras: {
            tittle: "las Etapas",
            labels: [
            "Etapa 1 - Planeación de Arranque",
            "Etapa 2 – Instalación Infraestructura Servidores",
            "Etapa 2 – Instalación Infraestructura Pc´s",
            "Etapa 3   Transferencia Operativa",
            ],
            data: [0,0,0,0,0,0]
        },
        ActividadSeleccionada:null,



    },
    mutations: {
        setActividades(state, actividades){
            state.Actividades= actividades;
        },
        setAvances(state, data){
            state.pastel.data= data.totales;
            state.barras.data= data.avances;
        },
        setActividad(state, item){
            state.ActividadSeleccionada = item;
        },
    },
    actions: {
        getActividades(context){
            axios.get('/actividad')
                    .then(
                        response => {
                            context.commit('setActividades',response.data);
                        }
                    )
        },
        getAvances(context){
            axios.post('/avances-actividades')
                    .then(
                        response => {
                            context.commit('setAvances',response.data);
                        }
                    )
        },
    },
    getters: {

    }

});
