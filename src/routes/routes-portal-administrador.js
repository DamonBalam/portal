import Vue from "vue";
import Router from "vue-router";

import Dashboard from "../components/Administrador/Dashboard.vue"
import Tabla from "../components/Administrador/Tabla.vue"
import Registro from "../components/Administrador/Formulario.vue"
import Editar from "../components/Administrador/Editar.vue"

const routes = [
  { path: "*", redirect:'administrador' },
  { path: "/administrador", component: Dashboard },
  { path: "/administrador/tabla", component: Tabla },
  { path: "/administrador/registro", component: Registro },
  { path: "/administrador/editar", component: Editar },
];

Vue.use(Router);

export default new Router({
//   mode: "history",
  mode: 'history',
  linkActiveClass: 'active',
  base: document.querySelector('#app').getAttribute('data-path') || '/',
  scrollBehavior: () => ({ y: 0 }),
  routes,
});
