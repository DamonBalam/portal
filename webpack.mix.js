const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
.js('src/app/app-portal-login.js', 'public/js')
.js('src/app/app-portal-admin.js', 'public/js')
.js('src/app/app-portal.js', 'public/js');
